(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["blank-page-blank-page-module"],{

/***/ "./src/app/layout/blank-page/blank-page-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/blank-page/blank-page-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: BlankPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlankPageRoutingModule", function() { return BlankPageRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _blank_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./blank-page.component */ "./src/app/layout/blank-page/blank-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _blank_page_component__WEBPACK_IMPORTED_MODULE_2__["BlankPageComponent"]
    }
];
var BlankPageRoutingModule = /** @class */ (function () {
    function BlankPageRoutingModule() {
    }
    BlankPageRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BlankPageRoutingModule);
    return BlankPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/blank-page/blank-page.component.html":
/*!*************************************************************!*\
  !*** ./src/app/layout/blank-page/blank-page.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-md-12\">\r\n    <div class=\"jumbotron\">\r\n      <h1>Pega Deploy On Demand in any Cloud</h1>\r\n      <p>\r\n        Providing industry-leading infrastructure, enterprise-grade services, and operational excellence for Pega Platform and strategic\r\n        application solutions in the cloud.\r\n      </p>\r\n      <p>\r\n        <a href=\"javascript:void(0)\" class=\"btn btn-danger btn-lg\" role=\"button\">\r\n          <i class=\"fa fa-book\" aria-hidden=\"true\"></i> Learn more »</a\r\n        >\r\n      </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"card card-default\">\r\n  <div class=\"card-header\"><i class=\"fa fa-cloud fa-fw\"></i> Cloud Offerings</div>\r\n  <!-- /.card-header -->\r\n  <app-cloud-timeline></app-cloud-timeline>\r\n  <!-- /.card-body -->\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layout/blank-page/blank-page.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/layout/blank-page/blank-page.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn {\n  background-color: #ff6c2c;\n  border-color: #ff6c2c; }\n  .btn:hover {\n    background: #ff5a13;\n    color: #fff;\n    border-color: #ff6c2c; }\n  .btn:active {\n    box-shadow: 0 5px #666;\n    -webkit-transform: translateY(4px);\n            transform: translateY(4px); }\n  h1 {\n  color: #ff6c2c; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2JsYW5rLXBhZ2UvQzpcXFVzZXJzXFxBZG1pbmlzdHJhdG9yXFxEZXNrdG9wXFxQZWdhXFxwZWdhZGVwbG95b25kZW1hbmR1aS9zcmNcXGFwcFxcbGF5b3V0XFxibGFuay1wYWdlXFxibGFuay1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsMEJBQXdCO0VBQ3hCLHNCQUFxQixFQVl0QjtFQWZEO0lBS0ksb0JBQStCO0lBQy9CLFlBQVU7SUFDVixzQkFBcUIsRUFDdEI7RUFSSDtJQVdJLHVCQUFzQjtJQUN0QixtQ0FBMEI7WUFBMUIsMkJBQTBCLEVBQzNCO0VBR0g7RUFDRSxlQUFhLEVBQ2QiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvYmxhbmstcGFnZS9ibGFuay1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0blxyXG57XHJcbiAgYmFja2dyb3VuZC1jb2xvcjojZmY2YzJjO1xyXG4gIGJvcmRlci1jb2xvcjogI2ZmNmMyYztcclxuICAmOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQ6IGRhcmtlbigjZmY2YzJjLCA1JSk7XHJcbiAgICBjb2xvcjojZmZmO1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZmY2YzJjO1xyXG4gIH1cclxuICAmOmFjdGl2ZVxyXG4gIHtcclxuICAgIGJveC1zaGFkb3c6IDAgNXB4ICM2NjY7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoNHB4KTtcclxuICB9XHJcbiBcclxufVxyXG5oMXtcclxuICBjb2xvcjojZmY2YzJjO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/layout/blank-page/blank-page.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/blank-page/blank-page.component.ts ***!
  \***********************************************************/
/*! exports provided: BlankPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlankPageComponent", function() { return BlankPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BlankPageComponent = /** @class */ (function () {
    function BlankPageComponent() {
    }
    BlankPageComponent.prototype.ngOnInit = function () { };
    BlankPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-blank-page',
            template: __webpack_require__(/*! ./blank-page.component.html */ "./src/app/layout/blank-page/blank-page.component.html"),
            styles: [__webpack_require__(/*! ./blank-page.component.scss */ "./src/app/layout/blank-page/blank-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BlankPageComponent);
    return BlankPageComponent;
}());



/***/ }),

/***/ "./src/app/layout/blank-page/blank-page.module.ts":
/*!********************************************************!*\
  !*** ./src/app/layout/blank-page/blank-page.module.ts ***!
  \********************************************************/
/*! exports provided: BlankPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlankPageModule", function() { return BlankPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _blank_page_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./blank-page-routing.module */ "./src/app/layout/blank-page/blank-page-routing.module.ts");
/* harmony import */ var _blank_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./blank-page.component */ "./src/app/layout/blank-page/blank-page.component.ts");
/* harmony import */ var _cloud_timeline_cloud_timeline_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./cloud-timeline/cloud-timeline.component */ "./src/app/layout/blank-page/cloud-timeline/cloud-timeline.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var BlankPageModule = /** @class */ (function () {
    function BlankPageModule() {
    }
    BlankPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _blank_page_routing_module__WEBPACK_IMPORTED_MODULE_2__["BlankPageRoutingModule"]],
            declarations: [_blank_page_component__WEBPACK_IMPORTED_MODULE_3__["BlankPageComponent"], _cloud_timeline_cloud_timeline_component__WEBPACK_IMPORTED_MODULE_4__["CloudTimelineComponent"]]
        })
    ], BlankPageModule);
    return BlankPageModule;
}());



/***/ }),

/***/ "./src/app/layout/blank-page/cloud-timeline/cloud-timeline.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/layout/blank-page/cloud-timeline/cloud-timeline.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card-body\">\r\n  <ul class=\"timeline\">\r\n    <li>\r\n      <div class=\"timeline-badge warning\"><i class=\"fa fa-amazon\"></i></div>\r\n      <div class=\"timeline-panel \">\r\n        <div class=\"timeline-heading\">\r\n          <h4 class=\"timeline-title\">Pega on AWS Cloud</h4>\r\n          <p>\r\n            <small class=\"text-muted\"> Amazon Web Services <i class=\"fa fa-info\"></i> </small>\r\n          </p>\r\n        </div>\r\n        <div class=\"timeline-body\">\r\n          <p>\r\n            Amazon Web Services is a subsidiary of Amazon.com that provides on-demand cloud computing platforms to individuals, companies\r\n            and governments, on a paid subscription basis. The technology allows subscribers to have at their disposal a virtual cluster of\r\n            computers, available all the time, through the Internet.\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </li>\r\n    <li class=\"timeline-inverted\">\r\n      <div class=\"timeline-badge danger\"><i class=\"fa fa-google\"></i></div>\r\n      <div class=\"timeline-panel\">\r\n        <div class=\"timeline-heading\">\r\n          <h4 class=\"timeline-title\">Pega on GCP Cloud</h4>\r\n          <p>\r\n            <small class=\"text-muted\"> Google Cloud Platform <i class=\"fa fa-info\"></i> </small>\r\n          </p>\r\n        </div>\r\n        <div class=\"timeline-body\">\r\n          <p>\r\n            Google Cloud Platform, offered by Google, is a suite of cloud computing services that runs on the same infrastructure that\r\n            Google uses internally for its end-user products, such as Google Search and YouTube\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </li>\r\n    <li>\r\n      <div class=\"timeline-badge primary\"><i class=\"fa fa-windows\"></i></div>\r\n      <div class=\"timeline-panel\">\r\n        <div class=\"timeline-heading\">\r\n          <h4 class=\"timeline-title\">Pega on Azure Cloud</h4>\r\n          <p>\r\n            <small class=\"text-muted\"> Microsoft Azure <i class=\"fa fa-info\"></i> </small>\r\n          </p>\r\n        </div>\r\n        <div class=\"timeline-body\">\r\n          <p>\r\n            Microsoft Azure is a cloud computing service created by Microsoft for building, testing, deploying, and managing applications\r\n            and services through a global network of Microsoft-managed data centers.\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layout/blank-page/cloud-timeline/cloud-timeline.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/layout/blank-page/cloud-timeline/cloud-timeline.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".timeline {\n  position: relative;\n  padding: 20px 0 20px;\n  list-style: none; }\n\n.timeline:before {\n  content: \" \";\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 50%;\n  width: 3px;\n  margin-left: -1.5px;\n  background-color: #eeeeee; }\n\n.timeline > li {\n  position: relative;\n  margin-bottom: 20px; }\n\n.timeline > li:before,\n.timeline > li:after {\n  content: \" \";\n  display: table; }\n\n.timeline > li:after {\n  clear: both; }\n\n.timeline > li:before,\n.timeline > li:after {\n  content: \" \";\n  display: table; }\n\n.timeline > li:after {\n  clear: both; }\n\n.timeline > li > .timeline-panel {\n  float: left;\n  position: relative;\n  width: 46%;\n  padding: 20px;\n  border: 1px solid #d4d4d4;\n  border-radius: 2px;\n  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175); }\n\n.timeline > li > .timeline-panel:before {\n  content: \" \";\n  display: inline-block;\n  position: absolute;\n  top: 26px;\n  right: -15px;\n  border-top: 15px solid transparent;\n  border-right: 0 solid #ccc;\n  border-bottom: 15px solid transparent;\n  border-left: 15px solid #ccc; }\n\n.timeline > li > .timeline-panel:after {\n  content: \" \";\n  display: inline-block;\n  position: absolute;\n  top: 27px;\n  right: -14px;\n  border-top: 14px solid transparent;\n  border-right: 0 solid #fff;\n  border-bottom: 14px solid transparent;\n  border-left: 14px solid #fff; }\n\n.timeline > li > .timeline-badge {\n  z-index: 100;\n  position: absolute;\n  top: 16px;\n  left: 50%;\n  width: 50px;\n  height: 50px;\n  margin-left: -25px;\n  border-radius: 50% 50% 50% 50%;\n  text-align: center;\n  font-size: 1.4em;\n  line-height: 50px;\n  color: #fff;\n  background-color: #999999; }\n\n.timeline > li.timeline-inverted > .timeline-panel {\n  float: right; }\n\n.timeline > li.timeline-inverted > .timeline-panel:before {\n  right: auto;\n  left: -15px;\n  border-right-width: 15px;\n  border-left-width: 0; }\n\n.timeline > li.timeline-inverted > .timeline-panel:after {\n  right: auto;\n  left: -14px;\n  border-right-width: 14px;\n  border-left-width: 0; }\n\n.timeline-badge.primary {\n  background-color: #2e6da4 !important; }\n\n.timeline-badge.success {\n  background-color: #3f903f !important; }\n\n.timeline-badge.warning {\n  background-color: #f0ad4e !important; }\n\n.timeline-badge.danger {\n  background-color: #d9534f !important; }\n\n.timeline-badge.info {\n  background-color: #5bc0de !important; }\n\n.timeline-title {\n  margin-top: 0;\n  color: inherit; }\n\n.timeline-body > p,\n.timeline-body > ul {\n  margin-bottom: 0; }\n\n.timeline-body > p + p {\n  margin-top: 5px; }\n\n@media (max-width: 767px) {\n  ul.timeline:before {\n    left: 40px; }\n  ul.timeline > li > .timeline-panel {\n    width: calc(100% - 90px);\n    width: -webkit-calc(100% - 90px); }\n  ul.timeline > li > .timeline-badge {\n    top: 16px;\n    left: 15px;\n    margin-left: 0; }\n  ul.timeline > li > .timeline-panel {\n    float: right; }\n  ul.timeline > li > .timeline-panel:before {\n    right: auto;\n    left: -15px;\n    border-right-width: 15px;\n    border-left-width: 0; }\n  ul.timeline > li > .timeline-panel:after {\n    right: auto;\n    left: -14px;\n    border-right-width: 14px;\n    border-left-width: 0; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2JsYW5rLXBhZ2UvY2xvdWQtdGltZWxpbmUvQzpcXFVzZXJzXFxBZG1pbmlzdHJhdG9yXFxEZXNrdG9wXFxQZWdhXFxwZWdhZGVwbG95b25kZW1hbmR1aS9zcmNcXGFwcFxcbGF5b3V0XFxibGFuay1wYWdlXFxjbG91ZC10aW1lbGluZVxcY2xvdWQtdGltZWxpbmUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBa0I7RUFDbEIscUJBQW9CO0VBQ3BCLGlCQUFnQixFQUNqQjs7QUFFRDtFQUNFLGFBQVk7RUFDWixtQkFBa0I7RUFDbEIsT0FBTTtFQUNOLFVBQVM7RUFDVCxVQUFTO0VBQ1QsV0FBVTtFQUNWLG9CQUFtQjtFQUNuQiwwQkFBeUIsRUFDMUI7O0FBRUQ7RUFDRSxtQkFBa0I7RUFDbEIsb0JBQW1CLEVBQ3BCOztBQUVEOztFQUVFLGFBQVk7RUFDWixlQUFjLEVBQ2Y7O0FBRUQ7RUFDRSxZQUFXLEVBQ1o7O0FBRUQ7O0VBRUUsYUFBWTtFQUNaLGVBQWMsRUFDZjs7QUFFRDtFQUNFLFlBQVcsRUFDWjs7QUFFRDtFQUNFLFlBQVc7RUFDWCxtQkFBa0I7RUFDbEIsV0FBVTtFQUNWLGNBQWE7RUFDYiwwQkFBeUI7RUFDekIsbUJBQWtCO0VBRWxCLDJDQUF1QyxFQUN4Qzs7QUFFRDtFQUNFLGFBQVk7RUFDWixzQkFBcUI7RUFDckIsbUJBQWtCO0VBQ2xCLFVBQVM7RUFDVCxhQUFZO0VBQ1osbUNBQWtDO0VBQ2xDLDJCQUEwQjtFQUMxQixzQ0FBcUM7RUFDckMsNkJBQTRCLEVBQzdCOztBQUVEO0VBQ0UsYUFBWTtFQUNaLHNCQUFxQjtFQUNyQixtQkFBa0I7RUFDbEIsVUFBUztFQUNULGFBQVk7RUFDWixtQ0FBa0M7RUFDbEMsMkJBQTBCO0VBQzFCLHNDQUFxQztFQUNyQyw2QkFBNEIsRUFDN0I7O0FBRUQ7RUFDRSxhQUFZO0VBQ1osbUJBQWtCO0VBQ2xCLFVBQVM7RUFDVCxVQUFTO0VBQ1QsWUFBVztFQUNYLGFBQVk7RUFDWixtQkFBa0I7RUFDbEIsK0JBQThCO0VBQzlCLG1CQUFrQjtFQUNsQixpQkFBZ0I7RUFDaEIsa0JBQWlCO0VBQ2pCLFlBQVc7RUFDWCwwQkFBeUIsRUFDMUI7O0FBRUQ7RUFDRSxhQUFZLEVBQ2I7O0FBRUQ7RUFDRSxZQUFXO0VBQ1gsWUFBVztFQUNYLHlCQUF3QjtFQUN4QixxQkFBb0IsRUFDckI7O0FBRUQ7RUFDRSxZQUFXO0VBQ1gsWUFBVztFQUNYLHlCQUF3QjtFQUN4QixxQkFBb0IsRUFDckI7O0FBRUQ7RUFDRSxxQ0FBb0MsRUFDckM7O0FBRUQ7RUFDRSxxQ0FBb0MsRUFDckM7O0FBRUQ7RUFDRSxxQ0FBb0MsRUFDckM7O0FBRUQ7RUFDRSxxQ0FBb0MsRUFDckM7O0FBRUQ7RUFDRSxxQ0FBb0MsRUFDckM7O0FBRUQ7RUFDRSxjQUFhO0VBQ2IsZUFBYyxFQUNmOztBQUVEOztFQUVFLGlCQUFnQixFQUNqQjs7QUFFRDtFQUNFLGdCQUFlLEVBQ2hCOztBQUVEO0VBQ0U7SUFDSSxXQUFVLEVBQ2I7RUFFRDtJQUNJLHlCQUF3QjtJQUV4QixpQ0FBZ0MsRUFDbkM7RUFFRDtJQUNJLFVBQVM7SUFDVCxXQUFVO0lBQ1YsZUFBYyxFQUNqQjtFQUVEO0lBQ0ksYUFBWSxFQUNmO0VBRUQ7SUFDSSxZQUFXO0lBQ1gsWUFBVztJQUNYLHlCQUF3QjtJQUN4QixxQkFBb0IsRUFDdkI7RUFFRDtJQUNJLFlBQVc7SUFDWCxZQUFXO0lBQ1gseUJBQXdCO0lBQ3hCLHFCQUFvQixFQUN2QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L2JsYW5rLXBhZ2UvY2xvdWQtdGltZWxpbmUvY2xvdWQtdGltZWxpbmUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudGltZWxpbmUge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBwYWRkaW5nOiAyMHB4IDAgMjBweDtcclxuICBsaXN0LXN0eWxlOiBub25lO1xyXG59XHJcblxyXG4udGltZWxpbmU6YmVmb3JlIHtcclxuICBjb250ZW50OiBcIiBcIjtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAwO1xyXG4gIGJvdHRvbTogMDtcclxuICBsZWZ0OiA1MCU7XHJcbiAgd2lkdGg6IDNweDtcclxuICBtYXJnaW4tbGVmdDogLTEuNXB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWU7XHJcbn1cclxuXHJcbi50aW1lbGluZSA+IGxpIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLnRpbWVsaW5lID4gbGk6YmVmb3JlLFxyXG4udGltZWxpbmUgPiBsaTphZnRlciB7XHJcbiAgY29udGVudDogXCIgXCI7XHJcbiAgZGlzcGxheTogdGFibGU7XHJcbn1cclxuXHJcbi50aW1lbGluZSA+IGxpOmFmdGVyIHtcclxuICBjbGVhcjogYm90aDtcclxufVxyXG5cclxuLnRpbWVsaW5lID4gbGk6YmVmb3JlLFxyXG4udGltZWxpbmUgPiBsaTphZnRlciB7XHJcbiAgY29udGVudDogXCIgXCI7XHJcbiAgZGlzcGxheTogdGFibGU7XHJcbn1cclxuXHJcbi50aW1lbGluZSA+IGxpOmFmdGVyIHtcclxuICBjbGVhcjogYm90aDtcclxufVxyXG5cclxuLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWwge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogNDYlO1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q0ZDRkNDtcclxuICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDFweCA2cHggcmdiYSgwLDAsMCwwLjE3NSk7XHJcbiAgYm94LXNoYWRvdzogMCAxcHggNnB4IHJnYmEoMCwwLDAsMC4xNzUpO1xyXG59XHJcblxyXG4udGltZWxpbmUgPiBsaSA+IC50aW1lbGluZS1wYW5lbDpiZWZvcmUge1xyXG4gIGNvbnRlbnQ6IFwiIFwiO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAyNnB4O1xyXG4gIHJpZ2h0OiAtMTVweDtcclxuICBib3JkZXItdG9wOiAxNXB4IHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gIGJvcmRlci1yaWdodDogMCBzb2xpZCAjY2NjO1xyXG4gIGJvcmRlci1ib3R0b206IDE1cHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgYm9yZGVyLWxlZnQ6IDE1cHggc29saWQgI2NjYztcclxufVxyXG5cclxuLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWw6YWZ0ZXIge1xyXG4gIGNvbnRlbnQ6IFwiIFwiO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAyN3B4O1xyXG4gIHJpZ2h0OiAtMTRweDtcclxuICBib3JkZXItdG9wOiAxNHB4IHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gIGJvcmRlci1yaWdodDogMCBzb2xpZCAjZmZmO1xyXG4gIGJvcmRlci1ib3R0b206IDE0cHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgYm9yZGVyLWxlZnQ6IDE0cHggc29saWQgI2ZmZjtcclxufVxyXG5cclxuLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtYmFkZ2Uge1xyXG4gIHotaW5kZXg6IDEwMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAxNnB4O1xyXG4gIGxlZnQ6IDUwJTtcclxuICB3aWR0aDogNTBweDtcclxuICBoZWlnaHQ6IDUwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IC0yNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJSA1MCUgNTAlIDUwJTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAxLjRlbTtcclxuICBsaW5lLWhlaWdodDogNTBweDtcclxuICBjb2xvcjogI2ZmZjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjOTk5OTk5O1xyXG59XHJcblxyXG4udGltZWxpbmUgPiBsaS50aW1lbGluZS1pbnZlcnRlZCA+IC50aW1lbGluZS1wYW5lbCB7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG4udGltZWxpbmUgPiBsaS50aW1lbGluZS1pbnZlcnRlZCA+IC50aW1lbGluZS1wYW5lbDpiZWZvcmUge1xyXG4gIHJpZ2h0OiBhdXRvO1xyXG4gIGxlZnQ6IC0xNXB4O1xyXG4gIGJvcmRlci1yaWdodC13aWR0aDogMTVweDtcclxuICBib3JkZXItbGVmdC13aWR0aDogMDtcclxufVxyXG5cclxuLnRpbWVsaW5lID4gbGkudGltZWxpbmUtaW52ZXJ0ZWQgPiAudGltZWxpbmUtcGFuZWw6YWZ0ZXIge1xyXG4gIHJpZ2h0OiBhdXRvO1xyXG4gIGxlZnQ6IC0xNHB4O1xyXG4gIGJvcmRlci1yaWdodC13aWR0aDogMTRweDtcclxuICBib3JkZXItbGVmdC13aWR0aDogMDtcclxufVxyXG5cclxuLnRpbWVsaW5lLWJhZGdlLnByaW1hcnkge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyZTZkYTQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRpbWVsaW5lLWJhZGdlLnN1Y2Nlc3Mge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZjkwM2YgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRpbWVsaW5lLWJhZGdlLndhcm5pbmcge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGFkNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRpbWVsaW5lLWJhZGdlLmRhbmdlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q5NTM0ZiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGltZWxpbmUtYmFkZ2UuaW5mbyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzViYzBkZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGltZWxpbmUtdGl0bGUge1xyXG4gIG1hcmdpbi10b3A6IDA7XHJcbiAgY29sb3I6IGluaGVyaXQ7XHJcbn1cclxuXHJcbi50aW1lbGluZS1ib2R5ID4gcCxcclxuLnRpbWVsaW5lLWJvZHkgPiB1bCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMDtcclxufVxyXG5cclxuLnRpbWVsaW5lLWJvZHkgPiBwICsgcCB7XHJcbiAgbWFyZ2luLXRvcDogNXB4O1xyXG59XHJcblxyXG5AbWVkaWEobWF4LXdpZHRoOjc2N3B4KSB7XHJcbiAgdWwudGltZWxpbmU6YmVmb3JlIHtcclxuICAgICAgbGVmdDogNDBweDtcclxuICB9XHJcblxyXG4gIHVsLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWwge1xyXG4gICAgICB3aWR0aDogY2FsYygxMDAlIC0gOTBweCk7XHJcbiAgICAgIHdpZHRoOiAtbW96LWNhbGMoMTAwJSAtIDkwcHgpO1xyXG4gICAgICB3aWR0aDogLXdlYmtpdC1jYWxjKDEwMCUgLSA5MHB4KTtcclxuICB9XHJcblxyXG4gIHVsLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtYmFkZ2Uge1xyXG4gICAgICB0b3A6IDE2cHg7XHJcbiAgICAgIGxlZnQ6IDE1cHg7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gIH1cclxuXHJcbiAgdWwudGltZWxpbmUgPiBsaSA+IC50aW1lbGluZS1wYW5lbCB7XHJcbiAgICAgIGZsb2F0OiByaWdodDtcclxuICB9XHJcblxyXG4gIHVsLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWw6YmVmb3JlIHtcclxuICAgICAgcmlnaHQ6IGF1dG87XHJcbiAgICAgIGxlZnQ6IC0xNXB4O1xyXG4gICAgICBib3JkZXItcmlnaHQtd2lkdGg6IDE1cHg7XHJcbiAgICAgIGJvcmRlci1sZWZ0LXdpZHRoOiAwO1xyXG4gIH1cclxuXHJcbiAgdWwudGltZWxpbmUgPiBsaSA+IC50aW1lbGluZS1wYW5lbDphZnRlciB7XHJcbiAgICAgIHJpZ2h0OiBhdXRvO1xyXG4gICAgICBsZWZ0OiAtMTRweDtcclxuICAgICAgYm9yZGVyLXJpZ2h0LXdpZHRoOiAxNHB4O1xyXG4gICAgICBib3JkZXItbGVmdC13aWR0aDogMDtcclxuICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/layout/blank-page/cloud-timeline/cloud-timeline.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/layout/blank-page/cloud-timeline/cloud-timeline.component.ts ***!
  \******************************************************************************/
/*! exports provided: CloudTimelineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CloudTimelineComponent", function() { return CloudTimelineComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CloudTimelineComponent = /** @class */ (function () {
    function CloudTimelineComponent() {
    }
    CloudTimelineComponent.prototype.ngOnInit = function () {
    };
    CloudTimelineComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cloud-timeline',
            template: __webpack_require__(/*! ./cloud-timeline.component.html */ "./src/app/layout/blank-page/cloud-timeline/cloud-timeline.component.html"),
            styles: [__webpack_require__(/*! ./cloud-timeline.component.scss */ "./src/app/layout/blank-page/cloud-timeline/cloud-timeline.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CloudTimelineComponent);
    return CloudTimelineComponent;
}());



/***/ })

}]);
//# sourceMappingURL=blank-page-blank-page-module.js.map