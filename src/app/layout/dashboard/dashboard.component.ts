import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { LaunchService } from '../../launch.service';

export interface ServerDetails {
  envName: string;
  cloud: string;
  launchedOn: string;
  version: string;
  architecturetype: string;
  accessUrl: string;
  region: string;
  status: string
}

const ELEMENT_DATA: ServerDetails[] = [];
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
  constructor(private launch: LaunchService) { }

  ngOnInit() {
    this.list();
  }
  data;
  count = 0;
  error;
  displayedColumns: string[] = ['envName', 'cloud', 'launchedOn', 'region', 'status', 'version', 'architecturetype', 'accessUrl'];
  dataSource = ELEMENT_DATA;
  list() {
    this.launch.getData().subscribe(data => {
      this.data = data;
      this.dataSource = this.data.result;
      // console.log(this.data);
      console.log(data);

      this.count = this.dataSource.length;
    });
  }
  getError(error) {
    if (error) {
      alert(error);
    }
  }
}
