import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlankPageRoutingModule } from './blank-page-routing.module';
import { BlankPageComponent } from './blank-page.component';
import { CloudTimelineComponent } from './cloud-timeline/cloud-timeline.component'

@NgModule({
    imports: [CommonModule, BlankPageRoutingModule],
    declarations: [BlankPageComponent, CloudTimelineComponent]
})
export class BlankPageModule { }
