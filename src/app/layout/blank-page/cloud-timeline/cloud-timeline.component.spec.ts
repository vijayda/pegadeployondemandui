import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudTimelineComponent } from './cloud-timeline.component';

describe('CloudTimelineComponent', () => {
  let component: CloudTimelineComponent;
  let fixture: ComponentFixture<CloudTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloudTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
