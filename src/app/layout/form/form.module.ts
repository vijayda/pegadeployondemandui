import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form.component';
import { PageHeaderModule } from './../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxLoadingModule } from 'ngx-loading';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';

@NgModule({
  imports: [CommonModule, FormRoutingModule, PageHeaderModule, NgxLoadingModule, NgbModule, FormsModule, ReactiveFormsModule],
  declarations: [FormComponent]
})
export class FormModule {}
