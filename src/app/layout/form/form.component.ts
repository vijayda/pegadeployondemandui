import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { FormBuilder, FormGroup } from '../../../../node_modules/@angular/forms';
import { LaunchService } from '../../launch.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  animations: [routerTransition()]
})
export class FormComponent implements OnInit {
  @Input() id: number;
  myForm: FormGroup;
  loading = false;
  serverName = '';
  dataSource;
  Region;
  CloudVendor;
  PegaVersion;
  ApplicationServer;
  DbServer;
  MachineType;
  Architecture;
  launched = false;
  customLoadingTemplate;
  constructor(private formBuilder: FormBuilder, private launch: LaunchService) {
    this.createForm();
  }
  private createForm() {
    this.myForm = this.formBuilder.group({
      provider: '',
      orgName: '',
      stacka: '',
      instance: '',
      machinetype: '',
      architecturetype: '',
      region: '',
      version: '',
      server: '',
      dbserver: ''
    });
  }
  ngOnInit() {
    this.getDbConf();
  }
  error = '';
  launchclicked() {
    console.log('testing:' + this.myForm.value.provider + this.myForm.value.orgName + this.myForm.value.region +this.myForm.value.machinetype +this.myForm.value.architecturetype +this.myForm.value.version +this.myForm.value.server+this.myForm.value.dbserver);
    this.launched = true;
    this.serverName = this.myForm.value.orgName;
    this.launch.launchServer(this.myForm.value.provider, this.myForm.value.orgName, this.myForm.value.region, this.myForm.value.version,this.myForm.value.machinetype,this.myForm.value.architecturetype,this.myForm.value.server,this.myForm.value.dbserver).subscribe(
      data => {
        this.loading = false;
        console.log(data);
        this.launched = false;
        //alert('Pega Launched Successfully');
      },

      error => {
        console.log(error);
        this.error = error.message;
        this.loading = false;
        //alert(this.error)
        // alert('Pega Launch in progess.....');
      }
    );
  }
  getDbConf() {
    this.launch.getConf().subscribe(result => {
      this.dataSource = result;
      console.log("dataSource" + this.dataSource)
      this.Region = this.dataSource.LaunchConfiguration[0].region;
      console.log("Region" + JSON.stringify(this.Region));
      this.CloudVendor = this.dataSource.LaunchConfiguration[2].provider;
      console.log("CloudVersion" + this.CloudVendor)
      this.PegaVersion = this.dataSource.LaunchConfiguration[1].pega_version;
      console.log("pegaversion" + this.PegaVersion);
      this.ApplicationServer = this.dataSource.LaunchConfiguration[3].App_server;
      this.DbServer = this.dataSource.LaunchConfiguration[4].Db_server;
      this.MachineType = this.dataSource.LaunchConfiguration[5].installation_type;
      this.Architecture = this.dataSource.LaunchConfiguration[6].architecture;
    },
      error => {
        console.log("error" + error);
      }
    );
  }
}
