import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';

@NgModule({
  imports: [CommonModule, TranslateModule, LoginRoutingModule, FormsModule, NgbModule, ReactiveFormsModule],
  declarations: [LoginComponent]
})
export class LoginModule {}
