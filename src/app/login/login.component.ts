import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../router.animations';
import { FormBuilder, FormGroup, Validators } from '../../../node_modules/@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
  constructor(private translate: TranslateService, public router: Router, public formBuilder: FormBuilder) {
    this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();
    this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
  }
  loginForm: FormGroup;
  invalid = false;
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  get f() {
    return this.loginForm.controls;
  }
  onLoggedin() {
    console.log(this.f.username.value + this.f.password.value);
    if (this.f.username.value == 'admin' && this.f.password.value == 'admin@123') {
      localStorage.setItem('isLoggedin', 'true');
    } else {
      this.invalid = true;
      // alert('Invalid Username/Password');
      localStorage.removeItem('isLoggedin');
    }
  }
}
