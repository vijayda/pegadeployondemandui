import { Injectable } from '@angular/core';
import { HttpClient } from '../../node_modules/@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LaunchService {
  constructor(private http: HttpClient) {
    console.log('Launch Service Initialized...');
  }
  data;
  conf;
  
  launchServer(provider: string, orgName: string, region: string, version: string, machinetype:string,architecturetype:string,server:string,dbserver:string) {
    console.log('Instance Name:' + orgName);
    console.log('selected region:' + region);
    const headersBasic = { authorization: 'Basic cGVnYWFkbWluOnBlZ2ExMjMk' };
    const requestHeaders = { headers: new Headers(headersBasic) };
    return this.http.post<any>(`/api/launchPega?orgName=` + orgName + '&provider=' + provider + '&region=' + region + '&version=' + version +'&machinetype=' + machinetype + '&architecturetype=' + architecturetype + '&server=' + server + '&dbserver=' +dbserver, requestHeaders).pipe(data => {
      return data;
    });
  }
  getData() {
    return this.http.get('/api/db/pega_env').pipe(data => {
      this.data = data;
      return this.data;
    });
  }
  getConf() {
    return this.http.get('/api/getConf/conf').pipe(conf => {
      this.conf = conf;
      return conf;
    })
  }
}
